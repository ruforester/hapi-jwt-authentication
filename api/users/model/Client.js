'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clientModel = new Schema({
  username: { type: String, required: true, index: { unique: true } },
  UUID: { type: String, required: true, index: { unique: true } }
});

module.exports = mongoose.model('Client', clientModel);