'use strict';

const bcrypt = require('bcrypt-nodejs');
const Boom = require('boom');
const Client = require('../model/Client');
const createClientSchema = require('../schemas/createClient');
const verifyCredentials = require('../util/userFunctions').verifyCredentials;
const UUID = require('node-uuid');
// const verifyUniqueUser = require('../util/userFunctions').verifyUniqueUser;
// const createToken = require('../util/token');

// function hashPassword(password, cb) {
//   // Generate a salt at level 10 strength
//   bcrypt.genSalt(10, (err, salt) => {
//     bcrypt.hash(password, salt, null, (err, hash) => {
//       return cb(err, hash);
//     });
//   });
// }

module.exports = {
  method: 'POST',
  path: '/api/clients',
  config: {
    auth: {
      strategy: 'jwt'//,
      // scope: ['admin']
    },
    // Before the route handler runs, verify user credentials
    handler: (req, res) => {


      let client = new Client();
      client.UUID = UUID.v1();
      client.username = req.pre.user.username;
              client.save((err) => {
          if (err) {
            throw Boom.badRequest(err);
          }
          res({message: `Client ${client.UUID} for user ${client.username} created.`})
          // If the user is saved successfully, issue a JWT
          // res({ id_token: createToken(user) }).code(201);
        });

    },
    // Validate the payload against the Joi schema
    // validate: {
    //   payload: createClientSchema
    // }
  }
}
