'use strict';

const Client = require('../model/Client');
const Boom = require('boom');

module.exports = {
  method: 'GET',
  path: '/api/clients',
  config: {
    handler: (req, res) => {
      Client
        .find()
        // Deselect the version field
        .select('-__v')
        .exec((err, clients) => {
          if (err) {
            throw Boom.badRequest(err);
          }
          if (!clients.length) {
            throw Boom.notFound('No clients found!');
          }
          res(clients);
        })
    },
    // Add authentication to this route
    // The user must have a scope of `admin`
    auth: {
      strategy: 'jwt'//,
      // scope: ['admin']
    }
  }
}