'use strict';

const Joi = require('joi');

const createClientSchema = Joi.object({
  UUID: Joi.string(),
  username: Joi.string()
});

module.exports = createClientSchema;