'use strict';

const Hapi = require('hapi');
const Boom = require('boom');
const mongoose = require('mongoose');
const glob = require('glob');
const path = require('path');
const secret = require('./config');
const HapiAuthJwt = require('hapi-auth-jwt');
const Inert = require('inert');
const Nes = require('nes');

const server = new Hapi.Server();

// The connection object takes some
// configuration, including the port
server.connection({ port: 8080, routes: { cors: true } });

const dbUrl = 'mongodb://mongouser:aaazzz@ds053176.mlab.com:53176/ruforester';

server.register([HapiAuthJwt, Inert, Nes], (err) => {
  
  // We are giving the strategy a name of 'jwt'
  server.auth.strategy('jwt', 'jwt', 'required', {
    key: secret,
    verifyOptions: { algorithms: ['HS256'] }
  });
  
  // Look through the routes in
  // all the subdirectories of API
  // and create a new route for each
  glob.sync('api/users/routes/*.js', { 
    root: __dirname 
  }).forEach(file => {
    console.log(file);
    const route = require(path.join(__dirname, file));
    server.route(route);
  });
});

// Start the server
server.start((err) => {
  if (err) {
    throw err;
  }
  // Once started, connect to Mongo through Mongoose
const db = mongoose.connection;

  db.on('connecting', function() {
    console.log('connecting to MongoDB...');
  });

  db.on('error', function(error) {
    console.error('Error in MongoDb connection: ' + error);
    mongoose.disconnect();
  });
  db.on('connected', function() {
    console.log('MongoDB connected!');
  });
  db.once('open', function() {
    console.log('MongoDB connection opened!');
  });
  db.on('reconnected', function () {
    console.log('MongoDB reconnected!');
  });
  db.on('disconnected', function() {
    console.log('MongoDB disconnected!');
    mongoose.connect(dbUrl, {server:{auto_reconnect:true}});
  });

  mongoose.connect(dbUrl, {server:{auto_reconnect:true}}, (err) => {
    if (err) {
      throw err;
    }
    console.log('Connected to database.');
    console.log(`Server address: ${server.info.uri}`);
  });
});