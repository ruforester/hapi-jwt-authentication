module.exports = {
    method: 'GET',
    path: '/dashboard/{param*}',
    config: {
        auth: {
            strategy: 'jwt'
        },
        handler: {
            directory: {
                path: './dashboard'
            }
        }
    }
}